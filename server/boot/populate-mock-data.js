module.exports = function (app) {
  let Card = app.loopback.getModel('card');
  Card.create([
    {title: "card 1", description: "card 1 description goes here", details: "card 1 details goes here", image:"https://unsplash.it/500/500/"},
    {title: "card 2", description: "card 2 description goes here", details: "card 2 details goes here", image:"https://unsplash.it/511/511/"},
    {title: "card 3", description: "card 3 description goes here", details: "card 3 details goes here", image:"https://unsplash.it/502/502/"},
    {title: "card 4", description: "card 4 description goes here", details: "card 4 details goes here", image:"https://unsplash.it/503/503/"},
    {title: "card 5", description: "card 5 description goes here", details: "card 5 details goes here", image:"https://unsplash.it/504/504/"},
    {title: "card 6", description: "card 6 description goes here", details: "card 6 details goes here", image:"https://unsplash.it/505/505/"},
    {title: "card 7", description: "card 7 description goes here", details: "card 7 details goes here", image:"https://unsplash.it/506/506/"},
    {title: "card 8", description: "card 8 description goes here", details: "card 8 details goes here", image:"https://unsplash.it/508/508/"},
    {title: "card 9", description: "card 9 description goes here", details: "card 9 details goes here", image:"https://unsplash.it/501/501/"},
    {title: "card 10", description: "card 10 description goes here", details: "card 10 details goes here", image:"https://unsplash.it/502/502/"},
    {title: "card 11", description: "card 11 description goes here", details: "card 11 details goes here", image:"https://unsplash.it/511/511/"},
    {title: "card 12", description: "card 12 description goes here", details: "card 12 details goes here", image:"https://unsplash.it/500/500/"}
  ]);

};


