'use strict';

module.exports = function(app) {
  let path = require('path');

  app.get('/app', function(req, res) {
    res.sendFile(path.resolve('client/index.html'));
  });

}
