import React from 'react';
import axios from 'axios';
import config from '../config';

export default class Card extends React.Component {
  constructor(props) {
    super(props);
  }

  removeCard() {
    axios.delete(config.api + 'cards/' + this.props.details.id)
      .then(resp => {
        this.props.refreshData(); //refreshing parent's data - can be avoided using redux!
      })
  }

  render() {
    return <div className="col-lg--3 col-md-3 col-sm-6 col-xs-12 margin-bottom-10" onTouchStart={() => {
      this.classList.toggle('hover')
    }}>
      <div className="cover">
        <div className="front" style={{backgroundImage: 'url(' + this.props.details.image + ')'}}>
          <div className="inner">
            <p>{this.props.details.title}</p>
            <span>{this.props.details.description}</span>
          </div>
        </div>
        <div className="back">
          <div className="inner">
            <p>{this.props.details.details}</p>
            <button className="button info" onClick={this.removeCard.bind(this)}>Remove Card</button>
          </div>
        </div>
      </div>
    </div>
  }
}
