import React from 'react';
import Card from './card';
import axios from 'axios';
import config from '../config';
import 'bootstrap-css-only';
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cards: []
    }
  }

  getData() {
    axios.get(config.api + 'cards')
      .then((cards) => {
        this.setState({
            cards: cards.data
          }
        )
      });
  }

  componentDidMount() {
    this.getData();
  }

  generateCardList() {
    return this.state.cards.map(card => <Card key={card.id} refreshData={this.getData.bind(this)} details={card}/>);
  }

  render() {
    return <div className="container">
      <h1>React bip</h1>
      <div>
        {this.generateCardList()}
      </div>
    </div>
  }
}
