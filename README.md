# react-bip

# prerequisites to be installed for build

1. node
2. yarn

# steps to build

Run the following in terminal in the order:

1. git clone git@bitbucket.org:benerd/react-bip.git
2. cd react-bip
3. yarn
4. node .
5. Navigate to localhost:3000/app